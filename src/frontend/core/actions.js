export default function createActions({ tree }) {
  const actions = {
    fetchFeeds: async () => {
      const response = await fetch('/api/feeds');
      const { feeds } = await response.json();

      tree.set('feeds', feeds);
    },

    fetchArticles: async ({ force = false, lastFeed = null } = {}) => {
      tree.set('loading', true);

      const feed = tree.get('currentFeed');
      const query = `?feed=${ feed }&forceRefresh=${ force }`;
      const response = await fetch(`/api/unread${ query }`);
      const { counts, articles } = await response.json();

      if (lastFeed && counts[lastFeed]) {
        tree.set('currentFeed', lastFeed);

        return actions.fetchArticles();
      }

      tree.set('articlesCounts', counts);
      tree.set('articles', articles);
      tree.set('loading', false);
    },

    toggleFeed: feed => {
      const currentFeed = tree.get('currentFeed');

      tree.set('currentFeed', feed === currentFeed ? null : feed);

      actions.fetchArticles();
    },

    markAsRead: async length => {
      tree.set('loading', true);

      const feed = tree.get('currentFeed');
      const articles = length > 1 ?
        tree.get('articles').slice(0, length) :
        tree.get('articles');

      await fetch('/api/markread', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          articles: articles.map(art => art.id),
        }),
      });

      tree.set('currentFeed', null);
      actions.fetchArticles({ force: true, lastFeed: feed });
    },
  };

  return actions;
}
