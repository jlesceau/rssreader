import Baobab from 'baobab';

const tree = new Baobab({
  currentFeed: null,
  feeds: [],
  articles: [],
  articlesCounts: {},
  loading: false,
});

export default tree;
