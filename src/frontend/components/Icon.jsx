import React from 'react';

function Icon({ name }) {
  return (
    <svg>
      <use href={ `#icon-${ name }` } />
    </svg>
  );
}

export default Icon;
