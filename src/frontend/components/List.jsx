import dayjs from 'dayjs';
import React, { useContext } from 'react';
import { useBranch } from 'baobab-react/hooks';

import Icon from './Icon';

import AppContext from '../core/context';

function List() {
  /**
   * Context
   */
  const { actions } = useContext(AppContext);


  /**
   * Baobab State
   */
  const { articles } = useBranch({
    articles: ['articles'],
  });


  /**
   * Handlers
   */
  const markAsRead = length => actions.markAsRead(length);


  /**
   * Render
   */
  if (!articles.length) {
    return (
      <div className="list empty">
        <div>No article.</div>
      </div>
    )
  }

  return (
    <div className="list">{
      articles.map((article, i) => (
        <a
          key={ `${ article.feed }-${ article.id }` }
          className="article"
          href={ article.link }
          target="_blank"
        >
          <div
            className="img feed-img"
            style={{
              borderColor: `#${ article.color }`,
            }}
          >
            <img
              src={ `/api/images/${ article.feed }` }
              alt={ article.feed }
              title={ article.feed }
            />
          </div>

          <div className="content">
            <div
              className="title"
              title={ article.title }
            >{ article.title }</div>
            <div className="infos">
              <div className="meta">
                <div className="source">{ article.source }</div>
                <div className="date">{
                  dayjs(article.date).format('ddd DD MMM')
                }</div>
                <div className="time">{
                  dayjs(article.date).format('HH:mm')
                }</div>
              </div>
              <div className="description">{ article.description }</div>
            </div>
          </div>

          <div className="img article-img">
            <img
              src={ article.img }
              alt={ article.title }
              title={ article.title }
            />

            <button
              type="button"
              className="mark-read"
              title="Mark all articles until this one"
              onClick={
                e => {
                  e.stopPropagation();
                  e.preventDefault();
                  markAsRead(i + 1);
                }
              }
            >
              <Icon name="tick" />
            </button>
          </div>
        </a>
      ))
    }</div>
  );
}

export default List;
