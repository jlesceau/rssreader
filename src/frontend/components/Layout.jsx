import React from 'react';

import List from './List';
import Header from './Header';
import Loader from './Loader';
import Sprite from './Sprite';

function Layout() {
  /**
   * Render
   */
  return (
    <div className="layout">
      <Sprite />
      <Header />
      <List />
      <Loader />
    </div>
  );
}

export default Layout;
