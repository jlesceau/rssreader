import React from 'react';
import cls from 'classnames';
import { useBranch } from 'baobab-react/hooks';

import Icon from './Icon';

function Loader() {
  /**
   * Baobab State
   */
  const { loading } = useBranch({
    loading: ['loading'],
  });


  /**
   * Render
   */
  return (
    <div
      className={
        cls({
          loader: true,
          active: loading,
        })
      }
    >
      <Icon name="loading" />
    </div>
  );
}

export default Loader;
