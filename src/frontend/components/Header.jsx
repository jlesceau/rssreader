import cls from 'classnames';
import React, { useContext } from 'react';
import { useBranch } from 'baobab-react/hooks';

import Icon from './Icon';

import AppContext from '../core/context';

function Header() {
  /**
   * Context
   */
  const { actions } = useContext(AppContext);


  /**
   * Baobab State
   */
  const { feeds, counts, articles, currentFeed } = useBranch({
    feeds: ['feeds'],
    articles: ['articles'],
    counts: ['articlesCounts'],
    currentFeed: ['currentFeed'],
  });


  /**
   * Handlers
   */
  const filterFeed = feed => actions.toggleFeed(feed);
  const markAsRead = () => actions.markAsRead();
  const refreshFeeds = () => actions.fetchArticles({ force: true });


  /**
   * Render
   */
  return (
    <div className="header">
      <button
        type="button"
        className="action logo"
        onClick={ () => filterFeed(null) }
      >
        <img
          src={ new URL('../assets/logo.png', import.meta.url) }
          alt="RSS Reader"
          title="RSS Reader"
        />
      </button>

      <div className="feeds">{
        feeds.map(feed => (
          <button
            key={ feed }
            className={
              cls({
                feed: true,
                active: currentFeed === feed,
                'fade-out': currentFeed && currentFeed !== feed,
              })
            }
            type="button"
            onClick={ () => filterFeed(feed) }
          >
            <img
              src={ `/api/images/${ feed }` }
              alt={ feed }
              title={ feed }
            />

            {
              counts[feed] > 0 ?
                <span>{ counts[feed] }</span> :
                undefined
            }
          </button>
        ))
      }</div>

      <button
        type="button"
        className={ cls({ action: true, disabled: !articles.length }) }
        title={
          currentFeed ?
            `Mark the '${ currentFeed }' list as read` :
            'Mark all articles as read'
        }
        onClick={ () => markAsRead() }
      >
        <Icon name="tick" />
      </button>

      <button
        type="button"
        className="action"
        title="Refresh"
        onClick={ () => refreshFeeds() }
      >
        <Icon name="refresh" />
      </button>
    </div>
  );
}

export default Header;
