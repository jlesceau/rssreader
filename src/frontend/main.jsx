import React from 'react';
import { useRoot } from 'baobab-react/hooks';
import { createRoot } from 'react-dom/client';

import tree from './core/tree';
import AppContext from './core/context';
import createActions from './core/actions';

import Layout from './components/Layout';

const actions = createActions({ tree });
const context = {
  actions,
};

actions.fetchFeeds();
actions.fetchArticles();

const App = () => {
  const Root = useRoot(tree);

  return (
    <Root>
      <AppContext.Provider value={ context }>
        <Layout />
      </AppContext.Provider>
    </Root>
  );
};
const root = createRoot(document.querySelector('#root'));

root.render(<App />);

window.app = {
  tree,
  actions,
};
