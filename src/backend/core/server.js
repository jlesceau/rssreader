import express from 'express';

import logger from './logger.js';

function loadServer({ config, actions }) {
  const server = express();

  server.use(express.json());

  server.get('/images/:feed', (req, res) => {
    logger.debug('GET /images/:feed', req.params);

    const feed = config.feeds.filter(f => f.name === req.params.feed)[0];

    if (feed) {
      res.sendFile(feed.img, { root: config.assetsDirectory });
    } else {
      res
        .status(404)
        .send();
    }
  });
  server.get('/feeds', (req, res) => {
    logger.debug('GET /feeds');

    const feeds = config.feeds.map(feed => feed.name);

    res.send({ feeds });
  });
  server.get('/unread', async (req, res) => {
    logger.debug('GET /unread', req.query);

    const articles = await actions.getArticles({
      feed: req.query.feed !== 'null' ? req.query.feed : null,
      forceRefresh: req.query.forceRefresh === 'true',
    });
    const counts = actions.getArticlesCounts();

    res.send({
      counts,
      articles,
    });
  });
  server.post('/markread', async (req, res) => {
    logger.debug('POST /markread', req.body);

    await actions.markAsRead({ articles: req.body.articles });

    res.send();
  });

  return server;
}

export default loadServer;
