import rssParser from 'rss-to-json';
import { parseDocument } from 'htmlparser2';

const parsers = {
  ccl: (feed, item) => {
    const desc = parseDocument(item.description);

    return {
      id: `${ feed.name }-${ feed.source }-${ item.title }`,
      title: item.title,
      color: feed.color,
      date: item.published,
      link: item.link,
      description: desc.children
        .filter(el => el.type === 'tag' && el.name === 'p')[0]
        ?.children?.[0]?.data || '',
      img: desc.children
        .filter(el => el.type === 'tag' && el.name === 'img')[0]
        ?.attribs?.src,
      feed: feed.name,
      source: feed.source,
      read: false,
    };
  },
  motorsport: (feed, item) => {
    const index = item.description?.indexOf(`<a class='more' href='`);

    return {
      id: `${ feed.name }-${ feed.source }-${ item.id || item.title }`,
      title: item.title,
      color: feed.color,
      date: item.published,
      link: item.link,
      description: index >= 0 ?
        item.description.substring(0, index) :
        item.description,
      img: item.enclosures.filter(enc => enc.type.startsWith('image/'))[0]?.url,
      feed: feed.name,
      source: feed.source,
      read: false,
    };
  },
  numeriques: (feed, item) => {
    const index = item.content?.indexOf(`<br><img src`);

    return {
      id: `${ feed.name }-${ feed.source }-${ item.id || item.title }`,
      title: item.title,
      color: feed.color,
      date: item.created,
      link: item.link,
      description: index >= 0 ?
        item.content.substring(0, index) :
        item.content,
      img: item.media.thumbnail?.url,
      feed: feed.name,
      source: feed.source,
      read: false,
    };
  },
};

async function parseFeeds({ config }) {
  const calls = config.feeds
    .reduce((list, feed) => ([
      ...list,
      ...feed.sources.map(source => ({
        ...source,
        name: feed.name,
        source: source.name,
        color: feed.color,
      })),
    ]), [])
    .map(async feed => {
      const rss = await rssParser.parse(feed.url);

      return rss.items
        .map(item => parsers[feed.parser](feed, item))
        .filter(item => (
          !feed.exceptions
            || feed.exceptions.every(exc => !exc.test(item.title))
        ));
    });
  const articles = (await Promise.all(calls))
    .reduce((data, items) => [...data, ...items], []);

  return articles;
}

export default parseFeeds;
