import dayjs from 'dayjs';
import { JSONPreset } from 'lowdb/node';
import { promises as fs, constants } from 'fs';
import isSameOrAfter from 'dayjs/plugin/isSameOrAfter.js';

import logger from './logger.js';

dayjs.extend(isSameOrAfter);

async function loadDatabase({ config }) {
  const defaultData = {
    lastRefresh: 0,
    articles: [],
  };
  let db;

  try {
    await fs.access(config.databaseDirectory, constants.W_OK);
  } catch (ex) {
    logger.error('Cannot access database directory', ex);
    process.exit(1);
  }

  try {
    db = await JSONPreset(
      `${ config.databaseDirectory }/articles.json`,
      defaultData
    );
  } catch (ex) {
    logger.error('Cannot read the database file. Please fix or delete it', ex);
    process.exit(1);
  }

  logger.info('Loaded Database');

  const database = {
    getLastRefresh: () => db.data.lastRefresh,

    getArticles: feed => {
      if (feed) {
        return db.data.articles.filter(art => art.feed === feed && !art.read);
      }

      return db.data.articles.filter(art => !art.read);
    },

    getArticlesCounts: () => {
      return db.data.articles
        .filter(art => !art.read)
        .reduce((counts, art) => {
          counts[art.feed] = (counts[art.feed] || 0) + 1;

          return counts;
        }, {});
    },

    saveNewArticles: async articles => {
      const limit = dayjs()
        .subtract(14, 'days')
        .startOf('day');
      const list = [
        ...db.data.articles,
      ];

      articles.forEach(art => {
        const exists = list.some(a => a.id === art.id);

        if (!exists) {
          list.push(art);
        }
      });

      db.data.lastRefresh = Date.now();
      db.data.articles = list
        .sort((a, b) => a.date - b.date)
        .filter(article => dayjs(article.date).isSameOrAfter(limit));

      await db.write();
    },

    markAsRead: async ({ articles }) => {
      db.data.articles.forEach(art => {
        if (articles.includes(art.id)) {
          art.read = true;
        }
      });

      await db.write();
    },
  };

  return database;
}

export default loadDatabase;
