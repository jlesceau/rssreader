import Joi from 'joi';
import JSON5 from 'json5';
import { promises as fs, constants } from 'fs';

import logger from './logger.js';

const sourceSchema = Joi.object({
  name: Joi.string(),
  url: Joi.string(),
  parser: Joi
    .string()
    .valid('ccl', 'numeriques', 'motorsport'),
  exceptions: Joi
    .array()
    .items(
      Joi
        .array()
        .length(2)
        .items(Joi.string())
    )
    .optional(),
});
const schema = Joi
  .object({
    databaseDirectory: Joi.string(),
    assetsDirectory: Joi.string(),
    parseInterval: Joi
      .number()
      .integer()
      .min(1)
      .max(24),
    loggerLevel: Joi
      .string()
      .valid('debug', 'info', 'warn', 'error', 'quiet'),
    loggerPrefix: Joi
      .boolean(),
    serverPort: Joi
      .number()
      .integer(),
    feeds: Joi
      .array()
      .items(
        Joi.object({
          name: Joi.string(),
          img: Joi.string(),
          color: Joi
            .string()
            .alphanum()
            .length(6),
          sources: Joi
            .array()
            .min(1)
            .items(sourceSchema),
        })
      ),
  })
  .options({ presence: 'required' });

export default async function loadConfig(configPath) {
  let config;

  try {
    await fs.access(configPath, constants.W_OK);
  } catch (ex) {
    logger.error('Cannot read config file', ex);
    process.exit(1);
  }

  try {
    config = JSON5.parse(await fs.readFile(configPath, 'utf8'));
  } catch (ex) {
    logger.error('Config file error: invalid JSON5', ex);
    process.exit(1);
  }

  try {
    await schema.validateAsync(config);
  } catch (ex) {
    logger.error(
      'Config file error: invalid format',
      JSON.stringify(ex.details, null, '  ')
    );
    process.exit(1);
  }

  try {
    config.feeds.forEach((feed, i) => {
      feed.sources.forEach((source, j) => {
        if (source.exceptions) {
          config.feeds[i].sources[j].exceptions = source.exceptions
            .map(([reg, flags]) => (
              new RegExp(reg, flags)
            ));
        }
      });
    });
  } catch (ex) {
    logger.error('Config file error: invalid RegExp', ex);
    process.exit(1);
  }

  logger.info('Loaded Config');

  return config;
}
