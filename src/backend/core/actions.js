import logger from './logger.js';
import parseFeeds from './parser.js';

function loadActions({ config, db }) {
  const actions = {
    parseFeeds: async (auto = false) => {
      const lastRefresh = db.getLastRefresh();
      const skip = (Date.now() - lastRefresh) < 300000;
      const mode = auto ? 'AUTO' : 'MANUEL';

      logger.debug(`Parsing feeds (${ mode })`, skip ? 'skipped' : '');

      if (!skip) {
        const articles = await parseFeeds({ config });

        await db.saveNewArticles(articles);
      }
    },

    setupParserTask: () => {
      logger.info('Auto parsing set up');
      setInterval(() => {
        actions.parseFeeds(true);
      }, config.parseInterval * 3600000);
      actions.parseFeeds(true);
    },

    getArticles: async ({ feed, forceRefresh }) => {
      if (forceRefresh) {
        await actions.parseFeeds();
      }

      return db.getArticles(feed);
    },

    getArticlesCounts: () => db.getArticlesCounts(),

    markAsRead: async ({ articles }) => {
      await db.markAsRead({ articles });
    },
  };

  return actions;
}

export default loadActions;
