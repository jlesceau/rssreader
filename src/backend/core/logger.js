const levels = {
  debug: 1,
  info: 2,
  warn: 3,
  error: 4,
  quiet: 5,
};
const colors = {
  debug: '\x1b[35m',
  info: '\x1b[34m',
  warn: '\x1b[33m',
  error: '\x1b[31m',
};

class Logger {
  constructor() {
    this.level = 2;
    this.prefix = 'RSS Reader -';
    this.prefixEnabled = true;

    this.log('info', 'Loaded Logger');
  }

  setPrefix(enabled) {
    this.prefixEnabled = enabled;
  }

  setLevel(level) {
    this.level = levels[level] || levels.warn;

    this.log('info', 'Changed log level to', level);
  }

  log(level, ...args) {
    const date = new Date();
    const log = [
      date.toLocaleDateString(),
      date.toLocaleTimeString(),
      colors[level],
      level.toUpperCase(),
      '\x1b[0m',
      ...args,
    ];

    if (this.prefixEnabled) {
      log.unshift(this.prefix);
    }

    console.log(...log);
  }

  debug(...args) {
    if (this.level <= levels.debug) {
      this.log('debug', ...args);
    }
  }
  info(...args) {
    if (this.level <= levels.info) {
      this.log('info', ...args);
    }
  }
  warn(...args) {
    if (this.level <= levels.warn) {
      this.log('warn', ...args);
    }
  }
  error(txt, exception) {
    if (this.level <= levels.error) {
      this.log('error', txt, '\n', exception);
    }
  }
}

export default new Logger();
