import logger from './core/logger.js';
import loadConfig from './core/config.js';
import loadServer from './core/server.js';
import loadActions from './core/actions.js';
import loadDatabase from './core/database.js';

async function start() {
  const config = await loadConfig(process.argv[2]);

  logger.setPrefix(config.loggerPrefix);
  logger.setLevel(config.loggerLevel);

  const db = await loadDatabase({ config });
  const actions = loadActions({ config, db });
  const server = loadServer({ config, actions });

  actions.setupParserTask();
  server.listen(config.serverPort);
  logger.info(`Server listening on port ${ config.serverPort }`);
}

start();
