# Install dependencies

`npm i`

# Run the backend

Create an asset folder to store your feed images.
Create a database folder to store the db files.
Copy & fill the backend.json5 file in the defaultConfig folder.
Run the backend specifying the path to the config file:

`npm run backend ./local/config.json5`

# Run the frontend

`npm run frontend`
